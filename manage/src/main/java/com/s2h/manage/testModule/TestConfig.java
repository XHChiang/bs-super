package com.s2h.manage.testModule;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.s2h.manage.testModule.main.Main2;
import com.s2h.manage.testModule.main.Main3;

@Configuration
public class TestConfig {

	@Bean
	public Main2 main2(Main3 main3) {
		return new Main2(main3);
	}
}
