package com.s2h.manage.config.servlet.handler;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.s2h.api.entity.Result;
import com.s2h.api.error.BsError;

@ControllerAdvice
public class GlobalExceptionHandler {

	@ResponseBody
	@ExceptionHandler(value = Exception.class)
	public Result handException(Exception e) {
		e.printStackTrace();
		return new Result(BsError.INTERNAL_ERROR);
	}
}
