package com.s2h.manage.webProducts.usr.entity.vo;

import com.s2h.manage.webProducts.usr.entity.Usr;

public class MaleUsr extends Usr {
	private static final long serialVersionUID = -8712826475017500347L;

	private String nanXingTeZheng;

	public String getNanXingTeZheng() {
		return nanXingTeZheng;
	}

	public void setNanXingTeZheng(String nanXingTeZheng) {
		this.nanXingTeZheng = nanXingTeZheng;
	}

}
