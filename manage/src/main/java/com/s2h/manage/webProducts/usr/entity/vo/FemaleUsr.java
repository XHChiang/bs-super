package com.s2h.manage.webProducts.usr.entity.vo;

import com.s2h.manage.webProducts.usr.entity.Usr;

public class FemaleUsr extends Usr {
	private static final long serialVersionUID = -6094704652467828054L;

	private String nvXingTeZheng;

	public String getNvXingTeZheng() {
		return nvXingTeZheng;
	}

	public void setNvXingTeZheng(String nvXingTeZheng) {
		this.nvXingTeZheng = nvXingTeZheng;
	}

}
