package com.s2h.manage.webProducts.rpc.hessian.service;

import com.s2h.manage.webProducts.rpc.hessian.IHessian;

public interface IHessianUsrService extends IHessian {
	public String queryUsrs();

	public String sayHelloToSomeBody(String someBodyName);
}
