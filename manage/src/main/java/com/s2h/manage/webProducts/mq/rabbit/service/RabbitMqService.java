package com.s2h.manage.webProducts.mq.rabbit.service;

public interface RabbitMqService {
	public String receive();

	public void send(String msg);

}
