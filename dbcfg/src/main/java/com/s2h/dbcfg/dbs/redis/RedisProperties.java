package com.s2h.dbcfg.dbs.redis;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import com.s2h.api.entity.Entity;

@Component
@ConfigurationProperties(prefix = "db.redis")
public class RedisProperties extends Entity {
	private String nodes;
	private int maxRedirects;

	public String getNodes() {
		return nodes;
	}

	public void setNodes(String nodes) {
		this.nodes = nodes;
	}

	public int getMaxRedirects() {
		return maxRedirects;
	}

	public void setMaxRedirects(int maxRedirects) {
		this.maxRedirects = maxRedirects;
	}

}
