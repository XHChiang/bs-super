package com.s2h.dbcfg.dbs.mysql;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.druid.pool.DruidDataSource;
import com.s2h.dbcfg.properties.DbProperties;

@Configuration
public class BsDB {
	@Value("${db.mysql.bs.url}")
	String dbUrl;

	@Value("${db.mysql.bs.driver}")
	String dbDriver;

	@Value("${db.mysql.bs.username}")
	String dbUsername;

	@Value("${db.mysql.bs.password}")
	String dbPassword;

	@Bean(name = "bs_dataSource")
	public DataSource dataSource(DbProperties dbProperties) {
		DruidDataSource druidDataSource = new DruidDataSource();
		// 基本属性 user、password、driverclass、url
		druidDataSource.setUrl(dbUrl);
		druidDataSource.setUsername(dbUsername);
		druidDataSource.setPassword(dbPassword);
		druidDataSource.setDriverClassName(dbDriver);

		druidDataSource.setMaxActive(dbProperties.getMaxActive());// 最大连接数
		druidDataSource.setInitialSize(dbProperties.getInitialSize());// 初始化大小
		druidDataSource.setMinIdle(dbProperties.getMinIdle());// 最小连接数
		druidDataSource.setMaxWait(dbProperties.getMaxWait());// 配置获取连接等待超时的时间
		druidDataSource.setTimeBetweenEvictionRunsMillis(dbProperties.getTimeBetweenEvictionRunsMillis());// 多久才进行一次检测，检测需要关闭的空闲连接

		druidDataSource.setRemoveAbandoned(dbProperties.getRemoveAbandoned());// 超过时间限制是否回收
		druidDataSource.setRemoveAbandonedTimeout(dbProperties.getRemoveAbandonedTimeout());// 超过时间限制多长
		druidDataSource.setMinEvictableIdleTimeMillis(dbProperties.getMinEvictableIdleTimeMillis());// 连接在池中最小生存的时间，单位是毫秒

		// 用于测试
		druidDataSource.setValidationQuery(dbProperties.getValidationQuery());// 用来检测连接是否有效的sql，sql自定义
		druidDataSource.setTestOnBorrow(dbProperties.getTestOnBorrow());// 申请连接时执行validationQuery检测连接是否有效，配置为true会降低性能
		druidDataSource.setTestOnReturn(dbProperties.getTestOnReturn());// 归还连接时执行validationQuery检测连接是否有效，配置为true会降低性能

		return druidDataSource;
	}

}
